const FRIDGE_PATH = 'fridge.json';
const fs = require('fs');
const util = require('util');
const log = console.log;
const assert = require('assert');
import { MINIMUM_UNIT_PRICE, NEW_PURCHASE_THRESHOLD, PURCHASE_SLOTS, MAXIMUM_UNIT_PRICE} from './config';

function loadFridgeState() {
    if (!fs.existsSync(FRIDGE_PATH)) {
        log(`File ${FRIDGE_PATH} did not exist. Creating.`);
        saveFridgeState({
            soldUnits: 0,
            numberOfSlots: PURCHASE_SLOTS,
            slotBalance: 0,
            orderPriceNok: 0,
            fridgeAddress: "",
            minimumUnitPriceNok: MINIMUM_UNIT_PRICE,
            maximumUnitPriceNok: MAXIMUM_UNIT_PRICE,
        });
        return loadFridgeState();
    }
    const rawdata = fs.readFileSync(FRIDGE_PATH);
    const state = JSON.parse(rawdata);
    assert(state !== undefined);
    return state;
}

function saveFridgeState(state) {
    let data = JSON.stringify(state);
    fs.writeFileSync(FRIDGE_PATH, data);
}

export class FridgeState {
    state: any;

    constructor(address: string) {
        this.state = loadFridgeState();
        this.state.orderPriceNok = NEW_PURCHASE_THRESHOLD;
        this.state.fridgeAddress = address;
        this.state.numberOfSlots = PURCHASE_SLOTS;
        this.state.minimumUnitPriceNok = MINIMUM_UNIT_PRICE;
        this.state.maximumUnitPriceNok = MAXIMUM_UNIT_PRICE;
        log(`Loaded fridge state ${util.inspect(this.state)}.`);
    }

    isSufficientPayment(incomeFiat: number) {
        const errorBuffer = 1.0 - 0.1; // 10%
        return incomeFiat >= MINIMUM_UNIT_PRICE * errorBuffer;
    }

    onPaymentReceived(income: number, incomeFiat: number) {

        // Some users batch buy when using the binance wallet, because
        // binance wallet sucks. Assume overpayment is batch buys.
        let units = Math.floor(
            incomeFiat / ((NEW_PURCHASE_THRESHOLD / PURCHASE_SLOTS)))
        // belts and suspenders
        units = Math.max(1, units)

        this.state.soldUnits += units;
        this.state.slotBalance += income - income * 0.1;
        if (this.state.soldUnits >= this.state.numberOfSlots) {
            this.state.soldUnits = 0;
            this.state.slotBalance = 0;
            this.flush();
            return true;
        }
        this.flush();
        return false;
    }

    flush() {
        saveFridgeState(this.state);
    }

    get() {
        return this.state;
    }
}
